defmodule OTPProcessInfoBinOut do
  @moduledoc false

  @metrics [
    :memory,
    :reductions,
    :current_stacktrace,
    :min_bin_vheap_size,
    :current_function,
    :binary
  ]

  use GenServer

  def start_link(args) do
    GenServer.start_link(__MODULE__, args, name: __MODULE__)
  end

  @impl true
  def init(args) do
    output_folder = Keyword.get(arsg, :output_folder)

    File.mkdir_p!(output_folder)

    System.put_env("ERL_PROC_INFO_BIN_OUT", output_folder)

    {:ok, args, {:continue, :continue}}
  end

  @impl true
  def handle_info(:continue, state) do
    call_erlang_process_info(state)

    {:noreply, state}
  end

  @impl true
  def handle_continue(:continue, state) do
    call_erlang_process_info(state)

    {:noreply, state}
  end

  defp call_erlang_process_info(state) do
    metrics =
      state
      |> Keyword.get(:metrics, @metrics)
      |> MapSet.new()
      |> MapSet.put(:binary)
      |> MapSet.to_list()

    callback = Keyword.get(state, :callback, fn _ -> nil end)
    delay = Keyword.get(state, :delay, 5_000)

    data =
      :erlang.processes()
      |> Enum.map(fn pid ->
        {pid, :erlang.process_info(pid)}
      end)
      |> Enum.filter(fn {_pid, proc_data} ->
        Keyword.get(proc_data, :registered_name)
      end)
      |> Enum.reduce(%{}, fn {pid, proc_data}, acc ->
        proc_info = Process.info(pid, metrics)

        Map.put(acc, proc_data[:registered_name], proc_info)
      end)

    Process.send_after(self(), :continue, delay)

    callback.(data)
  end
end
