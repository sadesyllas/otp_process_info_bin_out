# otp_process_info_bin_out

An OTP patch to dump the refc binaries, of a process when probed by calling `:erlang.process_info(pid, [..., :binary, ...])`.

## Setup

```shell
# ubuntu deps
sudo apt-get install autoconf libncurses-dev build-essential g++ m4 unixodbc-dev libssl-dev libwxgtk3.0-dev libglu-dev fop xsltproc
git clone https://github.com/erlang/otp.git
cd otp
git checkout OTP-21.3.8.6
./otp_build autoconf
./configure
patch -p1 < /path/to/otp_process_info_bin_out.patch
sudo make install
```

## Example

```shell
ERL_PROC_INFO_BIN_OUT=/tmp/otp_process_info_bin_out iex
# inside IEx
iex(1)> Process.register(self, :foo) # optional
iex(1)> :erlang.process_info(self(), [:memory, :reductions, :current_stacktrace, :min_bin_vheap_size, :current_function, :binary])
iex(1)> Ctrl-c Ctrl-c
# outside IEx
cat /tmp/otp_process_info_bin_out/*
# The files in /tmp/otp_process_info_bin_out/
# are named after the process for which
# :erlang.process_info was called.
# If a process has a registered name,
# then that name will be used as the file name,
# otherwise the process's pid is used instead.
```
